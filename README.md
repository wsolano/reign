# Reign project
The app should have two separate components: the Server and the Client.
The server should take care of pulling the data into the database and expose an API for the
Angular client. The client should render a web page that lists the articles in chronological
order.

###**SERVER COMPONENT**
Once an hour, the server app should connect to this API which shows recently posted
articles about Node.js on Hacker News:
http://hn.algolia.com/api/v1/search_by_date?query=nodejs
The server app should insert the data from the API into a MongoDB database and also
define a REST API which the client will use to retrieve the data.

### **CLIENT COMPONENT**
The user should be able to view a web page which shows the most recent posts in date
order (newer first). They should be able to click the delete button to delete an individual
post from this view. One a post is deleted it should not reappear, even if the HN API returns
it. Also, keep an eye on how the date is presented on each row in the wireframes.

### **Installation**
Enter root directory and execute the **docker-compose up** command

At the end of the execution, three containers will be available and running:
1. The Client container (container-news-app).
2. The Server container (containes-news-server).
3. The Database container (container-mongodb).

### **Usage**
In http://localhost:3000/api/ you can see and testing all endpoints available in the server app.

In the first run the news database will be empty, and you'll have to wait about 30 minutes until the scheduled task is executed to populate the database.

For populate the database you can call http://localhost:3000/news/updateNews through the browser, or an application like postman, which is the same endpoint used by the cron job. It can also be called from swagger api http://localhost:3000/api/#/default/NewsController_updateNews press **Try it out** and **execute**.

After populate the database you can go to http://localhost:4200, you should see the application running, and the news list.
