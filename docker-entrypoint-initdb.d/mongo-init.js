print('Start #################################################################');

db = db.getSiblingDB('reign_db');
db.createUser(
    {
        user: 'reign',
        pwd: 'reign',
        roles: [{ role: 'readWrite', db: 'reign_db' }],
    },
);
db.createCollection('news');

print('END #################################################################');
