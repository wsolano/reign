import { News } from './news.entity';

export class NewsServiceMock {
  private news: News = {
    id: null,
    created_at: new Date('2020-12-06T17:49:49.000Z'),
    title: null,
    url: null,
    author: 'dekhn',
    points: null,
    story_text: null,
    comment_text: 'This is a comment',
    num_comments: null,
    story_id: 25323509,
    story_title:
      'More than 1,200 Google workers condemn firing of AI scientist Timnit Gebru',
    story_url:
      'https://www.theguardian.com/technology/2020/dec/04/timnit-gebru-google-ai-fired-diversity-ethics',
    parent_id: 25324353,
    created_at_i: 1607276989,
    objectID: '25325132',
    is_delete: false,
  };

  getNews() {
    return [this.news];
  }

  getOneNews() {
    return this.news;
  }
}
