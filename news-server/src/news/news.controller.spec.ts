import { Test, TestingModule } from '@nestjs/testing';
import { HttpModule } from '@nestjs/common';

import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { NewsRepository } from '../../dist/news/news.repository';
import { NewsServiceMock } from './news.service.mock';

describe('NewsController', () => {
  let newsController: NewsController;
  let newsService: NewsService;
  let newsRepository: NewsRepository;
  const newsServiceMock: NewsServiceMock = new NewsServiceMock();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [NewsController],
      providers: [NewsService, NewsRepository],
    }).compile();

    newsController = module.get<NewsController>(NewsController);
    newsService = module.get<NewsService>(NewsService);
    newsRepository = module.get<NewsRepository>(NewsRepository);
  });

  it('should be defined', () => {
    expect(newsController).toBeDefined();
  });

  describe('News', () => {
    it('Get news local database', () => {
      const loadDataResult = newsServiceMock.getNews();
      jest.spyOn(newsService, 'getNews').mockResolvedValue(loadDataResult);
      expect(newsController.getNews()).resolves.toBe(loadDataResult);
    });

    it('Get news by objectID from local database', () => {
      const loadDataResult = newsServiceMock.getNews();
      const findOneResult = newsServiceMock.getOneNews();
      jest.spyOn(newsService, 'getNews').mockResolvedValue(loadDataResult);
      expect(
        newsController.findNewsByObjectID(findOneResult.objectID),
      ).resolves.toBe(loadDataResult);
    });

    it('Get news from hacker news and save in local database', () => {
      const loadDataResult = newsServiceMock.getNews();
      jest
        .spyOn(newsService, 'loadDataFromHackerNews')
        .mockResolvedValue(loadDataResult);
      expect(newsController.updateNews()).resolves.toBe(loadDataResult);
    });

    it('Mark news delete by objectID', () => {
      const loadDataResult = newsServiceMock.getNews();
      const findOneResult = newsServiceMock.getOneNews();
      jest.spyOn(newsService, 'getNews').mockResolvedValue(loadDataResult);
      expect(newsController.deleteNews(findOneResult.objectID)).resolves.toBe(
        loadDataResult,
      );
    });
  });
});
