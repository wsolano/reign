import { Entity, ObjectID, ObjectIdColumn, Column } from 'typeorm';

@Entity('news')
export class News {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  objectID: string;

  @Column()
  created_at: Date;

  @Column()
  story_id: number;

  @Column()
  title?: string;

  @Column()
  story_title?: string;

  @Column()
  story_url: string;

  @Column()
  url: string;

  @Column()
  author: string;

  @Column()
  points: number;

  @Column()
  num_comments: number;

  @Column()
  parent_id: number;

  @Column()
  story_text: string;

  @Column()
  comment_text: string;

  @Column()
  created_at_i: number;

  @Column()
  is_delete: boolean;

  constructor(news?: Partial<News>) {
    Object.assign(this, news);
  }
}
