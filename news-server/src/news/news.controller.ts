import { Controller, Get, Logger, Param, Put } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ApiImplicitParam } from '@nestjs/swagger/dist/decorators/api-implicit-param.decorator';

import { NewsService } from './news.service';
import { News } from './news.entity';

@Controller('news')
export class NewsController {
  private readonly logger = new Logger(NewsController.name);

  constructor(private newsService: NewsService) {}

  @Get()
  async getNews(): Promise<News[]> {
    this.logger.log('getting news from reign database');
    return this.newsService.getNews();
  }

  @Get('updateNews')
  async updateNews(): Promise<Observable<any>> {
    this.logger.log(
      'getting news from hacker news api and saving in reign database',
    );
    return this.newsService.loadDataFromHackerNews();
  }

  @Get(':objectID')
  @ApiImplicitParam({
    name: 'objectID',
    type: String,
    description: 'This field represent the news object id',
  })
  async findNewsByObjectID(@Param('objectID') objectID): Promise<News> {
    this.logger.log(
      'getting news by objectID ' + objectID + 'from reign database',
    );
    return this.newsService.findNewsByObjectID(objectID);
  }

  @Put('/delete/:objectID')
  @ApiImplicitParam({
    name: 'objectID',
    type: String,
    description: 'This field represent the news object id',
  })
  async deleteNews(@Param('objectID') objectID): Promise<News> {
    this.logger.log('delete the news with objectID ' + objectID);
    return this.newsService.deleteNews(objectID);
  }
}
