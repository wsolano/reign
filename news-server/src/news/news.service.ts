import { HttpService, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { validate } from 'class-validator';
import { Cron, CronExpression } from '@nestjs/schedule';

import { News } from './news.entity';
import { NewsDto } from './news.dto';

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(News)
    private readonly newsRepository: MongoRepository<News>,
    private http: HttpService,
  ) {}

  async getNews(): Promise<News[]> {
    return await this.newsRepository.find({ is_delete: false });
  }

  async findNewsByObjectID(id: string): Promise<News> {
    return await this.newsRepository.findOne({ objectID: id });
  }

  async updateNews(id: string, newsUpdate: Partial<News>): Promise<News> {
    // Check if entity exists
    const news = await this.findNewsByObjectID(id);
    if (!news) {
      throw new NotFoundException();
    }
    return await this.newsRepository.save(_.merge(news, newsUpdate));
  }

  async deleteNews(id: string): Promise<News> {
    // Check if entity exists
    const news = await this.findNewsByObjectID(id);
    if (!news) {
      throw new NotFoundException();
    }
    news.is_delete = true;
    return await this.newsRepository.save(news);
  }

  async loadDataFromHackerNews(): Promise<any> {
    //Get news form hacker news
    const nodejsNews = await this.getNewsFromHackerNews();
    if (nodejsNews) {
      // List of news with incorrect fields
      const newsWithError: string[] = [];
      // List of new news saved
      const newsSaved: string[] = [];
      // List of already exist news
      const newsInDatabase: string[] = [];
      for (const news of nodejsNews) {
        // Check if entity already exists in local database
        const newsFormDatabase = await this.findNewsByObjectID(news.objectID);
        if (!newsFormDatabase) {
          const newsToSave = _.merge(new NewsDto(), news);
          newsToSave.is_delete = false;
          newsToSave.created_at = new Date(newsToSave.created_at);
          // Validate the news fields are correctly
          const validation = await validate(newsToSave);
          if (_.isEmpty(validation)) {
            // Save the news in database
            const result = await this.newsRepository.save(newsToSave);
            newsSaved.push(result);
          } else {
            // Add the news to list of news with incorrect fields
            newsWithError.push(newsToSave);
          }
        } else {
          // Add the news to list of already exist news
          newsInDatabase.push(newsFormDatabase.objectID);
        }
      }
      return {
        newsSaved: newsSaved,
        newsWithError: newsWithError,
        newsInDatabase: newsInDatabase,
      };
    } else {
      // Return this exception when hacker news api not returns news
      throw new NotFoundException();
    }
  }

  async getNewsFromHackerNews(): Promise<News[]> {
    const newsData = await this.http
      .get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .pipe(map((response) => response.data))
      .toPromise();
    if (!newsData) {
      throw new NotFoundException();
    }
    return newsData.hits;
  }

  @Cron(CronExpression.EVERY_30_MINUTES)
  async updateNewsCron() {
    await this.loadDataFromHackerNews();
  }
}
