import {
  IsNotEmpty,
  IsString,
  IsBoolean,
  IsNumber,
  IsOptional,
  IsDate,
} from 'class-validator';

export class NewsDto {
  @IsNotEmpty()
  objectID: string;

  @IsNotEmpty()
  @IsDate()
  created_at: Date;

  @IsOptional()
  @IsNumber()
  story_id: number;

  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  story_title: string;

  @IsOptional()
  @IsString()
  story_url: string;

  @IsOptional()
  @IsString()
  url: string;

  @IsNotEmpty()
  @IsBoolean()
  is_delete: boolean;
}
