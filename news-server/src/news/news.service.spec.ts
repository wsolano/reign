import { Test, TestingModule } from '@nestjs/testing';
import { HttpModule, NotFoundException } from '@nestjs/common';

import { NewsService } from './news.service';
import { NewsRepository } from '../../dist/news/news.repository';
import { NewsServiceMock } from './news.service.mock';

describe('NewsService', () => {
  let newsService: NewsService;
  let newsRepository: NewsRepository;
  const newsServiceMock: NewsServiceMock = new NewsServiceMock();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [NewsService, NewsRepository],
    }).compile();

    newsService = module.get<NewsService>(NewsService);
    newsRepository = module.get<NewsRepository>(NewsRepository);
  });

  it('should be defined', () => {
    expect(newsService).toBeDefined();
  });

  describe('loadDataFromHackerNews', () => {
    it('Get news from hacker news api and save in local database - already exist news in database', () => {
      newsRepository.findOne = jest.fn();
      const findOneResult = newsServiceMock.getOneNews();
      jest.spyOn(newsRepository, 'findOne').mockResolvedValue(findOneResult);
      const loadDataResult = newsServiceMock.getNews();
      jest
        .spyOn(newsService, 'getNewsFromHackerNews')
        .mockResolvedValue(loadDataResult);

      jest
        .spyOn(newsService, 'findNewsByObjectID')
        .mockResolvedValue(findOneResult);
      expect(newsService.loadDataFromHackerNews()).resolves.toBe(
        loadDataResult,
      );
    });

    it('Get news from hacker news api and save in local database - save news on database', () => {
      newsRepository.findOne = jest.fn();
      const findOneResult = newsServiceMock.getOneNews();
      const loadDataResult = newsServiceMock.getNews();

      jest.spyOn(newsRepository, 'findOne').mockResolvedValue(findOneResult);
      jest.spyOn(newsRepository, 'save').mockResolvedValue(findOneResult);
      jest
        .spyOn(newsService, 'getNewsFromHackerNews')
        .mockResolvedValue(loadDataResult);
      jest.spyOn(newsService, 'findNewsByObjectID').mockResolvedValue(null);

      expect(newsService.loadDataFromHackerNews()).resolves.toBe(
        loadDataResult,
      );
    });

    it('Get news from hacker news api and save in local database - find news with errors', () => {
      newsRepository.findOne = jest.fn();
      const findOneResult = newsServiceMock.getOneNews();
      findOneResult.created_at = undefined;
      jest.spyOn(newsRepository, 'findOne').mockResolvedValue(findOneResult);
      const loadDataResult = newsServiceMock.getNews();
      jest
        .spyOn(newsService, 'getNewsFromHackerNews')
        .mockResolvedValue(loadDataResult);
      jest.spyOn(newsService, 'findNewsByObjectID').mockResolvedValue(null);

      expect(newsService.loadDataFromHackerNews()).resolves.toBe(
        loadDataResult,
      );
    });

    it('Get news from hacker news api and save in local database - Not found news from hacker news api', () => {
      jest.spyOn(newsService, 'getNewsFromHackerNews').mockResolvedValue(null);
      expect(newsService.loadDataFromHackerNews()).resolves.toBe(
        NotFoundException,
      );
    });
  });

  describe('delete news', () => {
    it('Get news from local database and set is_delete true - the news exist in database', () => {
      const findOneResult = newsServiceMock.getOneNews();
      newsRepository.findOne = jest.fn();
      jest.spyOn(newsRepository, 'save').mockResolvedValue(findOneResult);
      const loadDataResult = newsServiceMock.getNews();
      jest
        .spyOn(newsService, 'findNewsByObjectID')
        .mockResolvedValue(findOneResult);
      expect(newsService.deleteNews(findOneResult.objectID)).resolves.toBe(
        loadDataResult,
      );
    });

    it('Get news from local database and set is_delete true - the news not exist in database', () => {
      const findOneResult = newsServiceMock.getOneNews();
      newsRepository.findOne = jest.fn();
      jest.spyOn(newsService, 'findNewsByObjectID').mockResolvedValue(null);
      expect(newsService.deleteNews(findOneResult.objectID)).resolves.toBe(
        NotFoundException,
      );
    });
  });
});
