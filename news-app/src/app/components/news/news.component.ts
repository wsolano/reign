import { Component, OnInit } from '@angular/core';
import { NewsService } from '../../service/news.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  public newsList: any;
  private currentDate: Date = new Date();
  private clickedTrash = false;

  constructor(private newsService: NewsService) { }

  ngOnInit(): void {
    this.getNews();
  }

  getNews(): void {
    this.newsService.getNews().subscribe(
      result => {
        this.newsList = result;
        this.newsList.forEach(news => {
          news.created_at = new Date(news.created_at);
          news.created_at_day = news.created_at.getDate();
          news.created_at_month = news.created_at.getMonth();
          news.created_at_year = news.created_at.getYear();
          news.dateTime = news.created_at.getTime();
        });
        this.newsList = _.orderBy(this.newsList, 'dateTime', 'desc');
      }
    );
  }

  openUrl(item: any) {
    if (!this.clickedTrash) {
      if (item.story_url){
        window.open(item.story_url, '_blank');
      }
      else if (item.url){
        window.open(item.url, '_blank');
      }
    }
    else{
      this.clickedTrash = false;
    }
  }

  deleteNews(item: any) {
    this.clickedTrash = true;
    item.is_deleted = true;
    this.newsService.deleteNews(item).subscribe(
      result => {
        console.log('Item Deleted ', result);
        _.remove(this.newsList, { objectID: item.objectID });
      }, error => {
        console.log('Can not delete the item ', item);
      }
    );
  }

}
