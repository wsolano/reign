import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  urlBase = environment.URL_NEWS_SERVER;

  constructor(private httpClient: HttpClient) { }

  getNews(): Observable<any> {
    const url = `${this.urlBase}/news`;
    return this.httpClient.get(url);
  }

  deleteNews(news: any): Observable<any> {
    const url = `${this.urlBase}/news/delete/` + news.objectID;
    return this.httpClient.put(url, {objectID: news.objectID });
  }

}
